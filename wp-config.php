<?php
/**
 * The base configurations of the WordPress.
 *
 * This file has the following configurations: MySQL settings, Table Prefix,
 * Secret Keys, WordPress Language, and ABSPATH. You can find more information
 * by visiting {@link http://codex.wordpress.org/Editing_wp-config.php Editing
 * wp-config.php} Codex page. You can get the MySQL settings from your web host.
 *
 * This file is used by the wp-config.php creation script during the
 * installation. You don't have to use the web site, you can just copy this file
 * to "wp-config.php" and fill in the values.
 *
 * @package WordPress
 */
 
// Include local configuration
if (file_exists(dirname(__FILE__) . '/local-config.php')) {
	include(dirname(__FILE__) . '/local-config.php');
}

// Global DB config
if (!defined('DB_NAME')) {
	define('DB_NAME', 'db142268_axient');
}
if (!defined('DB_USER')) {
	define('DB_USER', 'db142268');
}
if (!defined('DB_PASSWORD')) {
	define('DB_PASSWORD', 'fingered!');
}
if (!defined('DB_HOST')) {
	define('DB_HOST', 'internal-db.s142268.gridserver.com');
}

/** Database Charset to use in creating database tables. */
if (!defined('DB_CHARSET')) {
	define('DB_CHARSET', 'utf8');
}

/** The Database Collate type. Don't change this if in doubt. */
if (!defined('DB_COLLATE')) {
	define('DB_COLLATE', '');
}

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'v4<X]C(_K#EE-iG2+U[^a?F5QV2:X$ `NS)3+t;rU$8wd&<<KII)c(:76~3S,{F]');
define('SECURE_AUTH_KEY',  'a=* )rKH!{peSF@uzf[m]r2~yUJ#0ef}O=bk-sQ}s:#yMX oq7]Hr*^,l9j:HF7?');
define('LOGGED_IN_KEY',    'i@(+1ZqdfxWia7]!F}R~u4E-N$ 98>?3OV2A^<c50mmp*pW3^K+e^Ijuk6X}xa0-');
define('NONCE_KEY',        'x2@Gig=%(:j;RC*#R0wKI0@C~I;-#pDpx+4K``)(-9. z9UosKnZ@Zx[-|YD:DKi');
define('AUTH_SALT',        'TPdK/~I]Ji}tB{glju&~,edTRc%~bOr<hD0>;/GushKpqSbb~N^0mQ-^_FwqMEz>');
define('SECURE_AUTH_SALT', 'm^?75+*]T38/- :WCO;qD{aY<|gf`SwaxtR|:/}[.|4w&{#f3gWmhoXdd`&xU&AG');
define('LOGGED_IN_SALT',   'CX 5Q)_F@skA7muHs|G#:I^ZZ.@91qyk3B)2przJ+=c>.:W)*Sq)|Li-Xj<+!qJY');
define('NONCE_SALT',       'ttgJl%Vl*k:A:%|0:I#I8<HG#5y]=wUjp)E2|f=+Tr1*m-uAaF@x^)m.)%|4YgJ{');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each a unique
 * prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'sbm_';

/**
 * WordPress Localized Language, defaults to English.
 *
 * Change this to localize WordPress. A corresponding MO file for the chosen
 * language must be installed to wp-content/languages. For example, install
 * de_DE.mo to wp-content/languages and set WPLANG to 'de_DE' to enable German
 * language support.
 */
define('WPLANG', '');

define('CONCATENATE_SCRIPTS', false);

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 */
if (!defined('WP_DEBUG')) {
	define('WP_DEBUG', false);
}

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
