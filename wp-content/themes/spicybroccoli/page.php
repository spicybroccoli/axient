<?php
/**
 * The main template file.
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Twenty_Ten
 * @since Twenty Ten 1.0
 */

get_header(); ?>
	
	<section class="panel panel-header">
		<img src="<?php echo get_stylesheet_directory_uri(); ?>/images/header_img.jpg">
	</section><!-- .panel -->

	<section class="panel panel-grey">
		<div class="wrapper">
			<?php get_sidebar('page'); ?>

			<div class="content">
				<div class="content-wrapper">
					<?php while(have_posts()): the_post(); ?>
						<h1><?php the_title(); ?></h1>
						<?php echo apply_filters('the_content', wpautop(get_the_content())); ?>
					<?php endwhile; ?>
				</div><!-- .content-wrapper -->
			</div><!-- .content -->
		</div>
	</section><!-- .panel -->

<?php get_footer(); ?>