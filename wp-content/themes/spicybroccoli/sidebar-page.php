<div class="sidebar">
	<?php
		global $post;
		$parent = ($post->post_parent) ? get_post($post->post_parent) : $post;
	
		$children = get_children(array(
			'post_type'		=> 'page',
			'post_parent' 	=> $parent->ID,
			'orderby'   	=> 'menu_order',
			'order'         => 'ASC',
		)); 
	?>
	<?php if (count($children) > 0): ?>
		<ul class="sidebar-menu">
			<li><?php echo $parent->post_title; ?></li>
			<?php foreach($children as $child): ?>
				<?php $is_active = ($child->ID == $post->ID) ? true : false; ?>
				<li class="<?php echo ($is_active) ? 'active' : ''; ?>">
					<a href="<?php echo get_permalink($child->ID); ?>"><?php echo $child->post_title; ?></a>
				</li>
			<?php endforeach; ?>
		</ul>
	<?php endif; ?>
	<ul class="sidebar-menu sidebar-menu-associated-products">
		
		<?php $connected = new WP_Query( array(
		  'connected_type' => 'pages_to_products',
		  'connected_items' => get_queried_object(),
		  'nopaging' => true,
		  'orderby' => 'menu_order',
		  'order' => 'ASC'
		) );

		// Display connected pages
		if ( $connected->have_posts() ) : ?>
			<li>Associated Products</li>
			<?php while ( $connected->have_posts() ) : $connected->the_post(); ?>
				<li><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></li>
			<?php endwhile; ?>
		<?php endif; ?>
		<?php wp_reset_query(); 
		wp_reset_postdata(); ?>
	</ul>
	<ul class="sidebar-menu sidebar-menu-other-products">
	<?php 
	$exclude = array_map(function($item){
		return $item->ID;
	}, $connected->posts);
	?>
	<?php
	$other_products = new WP_Query(array(
		'post__not_in' => $exclude,
		'post_type' => 'product',
		'posts_per_page' => -1,
		'orderby' => 'menu_order',
		'order' => 'ASC'
	));
	?>
		<?php if ($other_products->have_posts()): ?>
			<li>Our Products</li>
			<?php while($other_products->have_posts()): $other_products->the_post(); ?>
				<li><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></li>
			<?php endwhile; ?>
		<?php endif; ?>
		<?php wp_reset_query(); 
		wp_reset_postdata(); ?>
	</ul>
</div><!-- .sidebar -->