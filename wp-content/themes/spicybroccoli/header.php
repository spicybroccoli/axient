<?php
/**
 * The Header for our theme.
 *
 * Displays all of the <head> section and everything up till <div id="main">
 *
 * @package WordPress
 * @subpackage Twenty_Ten
 * @since Twenty Ten 1.0
 */
?><!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>" />
<meta name="viewport" content="width=device-width, initial-scale=1">
<title><?php
	/*
	 * Print the <title> tag based on what is being viewed.
	 */
	global $page, $paged;

	wp_title( '|', true, 'right' );

	// Add the blog name.
	bloginfo( 'name' );

	// Add the blog description for the home/front page.
	$site_description = get_bloginfo( 'description', 'display' );
	if ( $site_description && ( is_home() || is_front_page() ) )
		echo " | $site_description";

	// Add a page number if necessary:
	if ( $paged >= 2 || $page >= 2 )
		echo ' | ' . sprintf( __( 'Page %s', 'twentyten' ), max( $paged, $page ) );

	?></title>
<link rel="profile" href="http://gmpg.org/xfn/11" />
<link rel="stylesheet" type="text/css" media="all" href="<?php bloginfo( 'stylesheet_url' ); ?>" />
<link rel="stylesheet" type="text/css" media="all" href="<?php echo get_stylesheet_directory_uri(); ?>/style-responsive.css" />
<link href='http://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,400,300,600' rel='stylesheet' type='text/css'>
<script src="<?php echo get_stylesheet_directory_uri(); ?>/js/modernizr.js"></script>

<!-- Go to www.addthis.com/dashboard to customize your tools -->
<script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-4fab20d5567ffc98" async="async"></script>
<?php wp_head(); ?>
</head>

<body <?php body_class(); ?> data-spy="scroll" data-target="#navbar">
<div id="mp-pusher" class="mp-pusher">
    <nav class="mp-menu" id="mp-menu">
		<div class="mp-level">    	
	        <?php
	           	wp_nav_menu( 
	            	array( 
	            		'menu' => 'primary',
	            		'container'  => 'false',
	            		'container_id' => 'mp-menu',
	            		'menu_class' => 'mp-level',
	            		'container_class' => 'mp-menu',
	            		'menu_class' => '',  
	            		'walker' => new SBM_Responsive_Nav()
	            	) 
	            ); 
	        ?>	
                    
        </div>
    </nav>

	<section id="header" class="panel SiteHeader <?php echo ( !is_front_page() ) ? 'SiteHeader__page' : ''; ?>">
		<div class="wrapper">
			<div id="top-menu">
				<a href="#" id="trigger" class="menu-trigger">Open/Close Menu</a>
			</div>
			<div class="SiteHeader--topnav">
				<ul>
					<li class="topnav--phone">+ 61 2 8338 3444</li>
					<li class="topnav--facebook"><a href="http://www.facebook.com" target="_blank">Facebook</a></li>
					<li class="topnav--linkedin"><a href="http://www.linkedin.com/company/axient-pty-ltd?trk=cp_followed_name_axient-pty-ltd" target="_blank">LinkedIn</a></li>
					<li class="topnav--slideshare"><a href="http://www.slideshare.net/Axient" target="_blank">SlideShare</a></li>
					<li class="topnav--youtube"><a href="https://www.youtube.com/user/AxientTube" target="_blank">YouTube</a></li>
				</ul>
			</div>
			<div class="SiteHeader--logo">
				<a href="<?php echo site_url(); ?>"><img src="<?php echo get_stylesheet_directory_uri(); ?>/images/logo.png"></a>
			</div>
			<div class="SiteHeader--menu">
				<?php wp_nav_menu( array( 'container_class' => 'menu-header', 'theme_location' => 'primary' ) ); ?>
			</div>
		</div><!-- .wrapper-->
	</section><!-- .SiteHeader -->