<?php
/**
 * Template Name: Testimonials
 * The main template file.
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Twenty_Ten
 * @since Twenty Ten 1.0
 */

get_header(); ?>

	<section class="panel panel-header">
        <img src="<?php echo get_stylesheet_directory_uri(); ?>/images/header_img.jpg" />
	</section><!-- .panel -->

	<section class="panel panel-grey">
		<div class="wrapper">
			<?php get_sidebar('blog'); ?>

			<div class="content">
				<div class="content-wrapper">
					<h1>Blog</h1>

					<?php /* If there are no posts to display, such as an empty archive page */ ?>
					<?php if ( ! have_posts() ) : ?>
						<div id="post-0" class="post error404 not-found">
							<h1 class="entry-title">Not Found</h1>
							<div class="entry-content">
								<p>Apologies, but no results were found for the requested archive. Perhaps searching will help find a related post.</p>
								<?php get_search_form(); ?>
							</div><!-- .entry-content -->
						</div><!-- #post-0 -->
					<?php endif; ?>

					<?php
						/* Start the Loop.
						 *
						 */ ?>

					<?php while ( have_posts() ) : the_post(); ?>
							<div id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
								<h4><a href="<?php the_permalink(); ?>" title="<?php echo esc_attr( sprintf( 'Permalink to %s', the_title_attribute( 'echo=0' ) ) ); ?>" rel="bookmark"><?php the_title(); ?></a></h4>

								<div class="entry-meta">
									<div class="entry-meta-author">
										<?php echo get_the_author(); ?>
									</div> <span class="meta-sep">/</span> 
									<div class="entry-meta-date">
										<?php echo get_the_date(); ?>
									</div> <span class="meta-sep">/></span> 

									<?php
										$tags_list = get_the_tag_list( '', ', ' );
										if ( $tags_list ):
									?>
										<span class="tag-links">
											<?php printf( '<span class="%1$s">Tagged</span> %2$s', 'entry-utility-prep entry-utility-prep-tag-links', $tags_list ); ?>
										</span>
										
									<?php endif; ?>
								</div><!-- .entry-meta -->

								<?php if ( is_archive() || is_search() ) : // Only display excerpts for archives and search.?>
									<?php 
										if ( has_post_thumbnail() ) { // check if the post has a Post Thumbnail assigned to it.
											the_post_thumbnail();
										} 
									?>
									<div class="entry-summary">
										<?php the_excerpt(); ?>
									</div><!-- .entry-summary -->
								<?php else : ?>
								<div class="entry-content">
									<?php 
										if ( has_post_thumbnail() ) { // check if the post has a Post Thumbnail assigned to it.
											the_post_thumbnail();
										} 
									?>
									<?php the_content( 'Continue reading <span class="meta-nav">&rarr;</span>'); ?>
								</div><!-- .entry-content -->
								<?php endif; ?>

								
							</div><!-- #post-## -->


					<?php endwhile; // End the loop. Whew. ?>
					<hr class="blog" />
					<div class="pagination">
						<ul class="pagination--numbers">
							<?php
								global $wp_query;

								   $big = 999999999; // This needs to be an unlikely integer

								   // For more options and info view the docs for paginate_links()
								   // http://codex.wordpress.org/Function_Reference/paginate_links
								   echo paginate_links( array(
								       'base' => str_replace( $big, '%#%', get_pagenum_link($big) ),
								       'current' => max( 1, get_query_var('paged') ),
								       'total' => $wp_query->max_num_pages,
								       'mid_size' => 5
								   ) );

							?>
						</ul>
					</div><!-- .pagination -->

		
				</div>
			</div>
		</div>
	</section>

<?php get_footer(); ?>