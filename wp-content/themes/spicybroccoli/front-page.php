<?php get_header(); ?>

	<?php echo do_shortcode("[metaslider id=87]"); ?>

	<section class="panel panel-productTeaser">
		<div class="wrapper">
			<h1>Featured Products</h1>

			<ul class="productTeaser">
				
				<?php $products = new WP_Query(array(
					'post_type' => 'product',
					'posts_per_page' => 4,
					'meta_key' => '_home_order',
					'orderby' => 'meta_value_num',
					'order' => 'ASC',
					'meta_query' => array(
                                array('key' => 'featured',
                                      'value' => '1',
                                )
                            )
					));
				?>

				<?php if ($products->have_posts()) while($products->have_posts()): $products->the_post(); ?>
				<li class="product <?php echo $post->post_name; ?>">
					<h3><?php echo get_post_meta(get_the_ID(), '_promo_title', true); ?></h3>
					<a href="<?php the_permalink(); ?>">
						<div class="product--video">
							<video>
								<?php if (get_post_meta(get_the_ID(), '_video_h264', true)): ?>
							    	<source src="<?php echo wp_get_attachment_url(get_post_meta(get_the_ID(), '_video_h264', true)); ?>" type="video/mp4">
								<?php endif; ?>
								<?php if (get_post_meta(get_the_ID(), '_video_vp8', true)): ?>
							    	<source src="<?php echo wp_get_attachment_url(get_post_meta(get_the_ID(), '_video_vp8', true)); ?>" type="video/webm">
								<?php endif; ?>
							</video>
							<div class="product--video--overlay">
								<span><?php echo get_post_meta(get_the_ID(), '_promo_video_overlay', true); ?></span>
							</div>
							<?php if (get_post_meta(get_the_ID(), '_video_thumb', true)): ?>
							<img class="product--video--image" src="<?php echo wp_get_attachment_url(get_post_meta(get_the_ID(), '_video_thumb', true)); ?>">
							<?php endif; ?>
						</div>
						<?php echo wpautop(get_post_meta(get_the_ID(), '_promo_desc', true)); ?>
						<span><?php echo get_post_meta(get_the_ID(), '_cta_label', true); ?></span>
					</a>
					</li>
				<?php endwhile; ?>
			</ul>
		</div><!-- .wrapper-->
	</section><!-- .panel -->

	<section class="panel panel-strip">
		<img src="<?php echo get_stylesheet_directory_uri(); ?>/images/strip.jpg">
		<?php echo get_post_meta(26, '_home_page_pull_quote', true); ?>

	</section><!-- .panel -->

	<section class="panel panel-sections">
		<div class="wrapper">
			<ul class="sectionsTeaser">
				<li class="sectionsTeaser--blog">
					<img class="sectionsTeaser--blog--image sectionsTeaser--image" src="<?php echo get_stylesheet_directory_uri(); ?>/images/cta_blog.png" />

					<h3>Featured Blog</h3>
					<?php 
						$featured = new WP_Query(array(
							'post_type' => 'post',
							'posts_per_page' => 1,
							'meta_key' => 'featured',
							'meta_value' => '1'
						));
					?>
					<?php while($featured->have_posts()): $featured->the_post(); ?>
						<h5><?php the_title(); ?></h5>
						<?php sbm_smart_excerpt(); ?>
						<a href="<?php the_permalink(); ?>" class="button">Read more</a>
					<?php endwhile; ?>
				</li>
				<li class="sectionsTeaser--support">
					<img class="sectionsTeaser--support--image sectionsTeaser--image" src="<?php echo get_stylesheet_directory_uri(); ?>/images/cta_support.png" />
					<h3>Our Support</h3>
					<p>You want to talk with certified experts, people who know their way around the product and its relationships in your IT environment. And we are that kind of organisation.<br />We are always there for you, 24/7.</p>
					<a href="http://dev.spicybroccolitesting.com.au/axient/about-us/technical-support" class="button">Get support</a>
				</li>
				<li class="sectionsTeaser--customers">
					<img class="sectionsTeaser--customers--image sectionsTeaser--image" src="<?php echo get_stylesheet_directory_uri(); ?>/images/cta_customers.png" />
					<h3>What our Customers Say</h3>
					<p>"Not only have we been able to provide an APAC solution, it fits into our global initiative. In 6 1⁄2 years, I don’t remember a time when the solution has fallen over."<span>Global Financial Services Company</span></p>
					<a href="http://dev.spicybroccolitesting.com.au/axient/about-us/customer-testimonials" class="button">Read more</a>
				</li>
			</ul>
		</div><!-- .wrapper-->
	</section><!-- .panel -->

	<section class="panel panel-lovedby">
		<div class="wrapper">
			<h2>Axient is Used and Loved by...</h2>
			<ul class="industries cf">
				<a href="<?php echo site_url('/my-industry/banking-financial-services'); ?>"><li>Banking & Financial Services</li></a>
				<a href="<?php echo site_url('/my-industry/government'); ?>"><li>Government</li></a>
				<a href="<?php echo site_url('/my-industry/supply-chain-logistics'); ?>"><li>Supply Chain & Logistics</li></a>
				<a href="<?php echo site_url('/my-industry/utilities'); ?>"><li>Utilities</li></a>
			</ul>
			<span class="including-span">Including</span>
			<hr />
			<ul class="including-clients cf">
				<li><img src="<?php echo get_stylesheet_directory_uri(); ?>/images/client_boq.png"></li>
				<li><img src="<?php echo get_stylesheet_directory_uri(); ?>/images/client_hcf.png"></li>
				<li><img src="<?php echo get_stylesheet_directory_uri(); ?>/images/client_tal.png"></li>
				<li><img src="<?php echo get_stylesheet_directory_uri(); ?>/images/client_veolia.png"></li>
            
            
            </ul>
		</div><!-- .wrapper-->
	</section><!-- .panel -->

	<section class="panel panel-contact">
		<div class="wrapper">
			<h2>Contact Us</h2>
			<p>Have a question or just want to get in touch?<br/>Drop us a line and we will get back to you shortly with any enquiries.</p>
			<?php echo do_shortcode("[contact-form-7 id=116 title=contact-form]"); ?>
		</div><!-- .wrapper-->
	</section><!-- .panel-contact -->

<?php get_footer() ?>