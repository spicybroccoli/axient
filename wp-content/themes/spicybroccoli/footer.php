<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the id=main div and all content
 * after. Calls sidebar-footer.php for bottom widgets.
 *
 * @package WordPress
 * @subpackage Twenty_Ten
 * @since Twenty Ten 1.0
 */
?>

	<section id="footer" class="panel SiteFooter" role="contentinfo">
		<div class="wrapper">
			<div class="SiteFooter--support">
				<h3>Technical Support</h3>
				<div><h4>Australia</h4>1300 850 945</div>
				<div><h4>New Zealand</h4>0800 898 881</div>
			</div><!-- .SiteFooter--suppport -->
			<?php wp_nav_menu( array( 'container_class' => 'menu-footer', 'theme_location' => 'footer' ) ); ?>
		</div><!-- .wrapper -->
	</section><!-- #footer -->
	<section id="colophon" class="panel SiteColophon" role="contentinfo">
		<div class="wrapper">
			<div><a href="<?php echo site_url('/contact-us'); ?>">Contact Us</a><a href="<?php echo site_url('/privacy-policy'); ?>">Privacy Policy</a><a href="<?php echo site_url('/terms-and-conditions'); ?>">Terms and Conditions</a><a href="<?php echo site_url('/blog'); ?>">Blog</a></div>
			<div>&copy; <?php echo date("Y"); ?> <?php bloginfo( 'name' ); ?> Pty Ltd. &middot; ABN: 14090510848 &middot; Address: Suite 601, Level 6, 28 Clarke Street, Crows Nest, NSW 2065, Australia &middot; Website Design by <a href="http://www.spicybroccoli.com" target="_blank" class="sbm">Spicy Broccoli Media</a>
</div>
		</div><!-- .wrapper -->

	</section><!-- #colophon -->

</div>

<?php wp_footer(); ?>

<script> new mlPushMenu( document.getElementById( 'mp-menu' ), document.getElementById( 'trigger' ) );</script>
</body>
</html>