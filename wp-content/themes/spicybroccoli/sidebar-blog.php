<div class="sidebar">
	<?php if (is_single()): ?>
	<ul class="sidebar-menu sidebar-menu-articles">
		<li>Recent Articles</li>
		<?php 
		$recent_articles = get_posts(array(
			'post_type'			=> 'post',
			'posts_per_page'	=> 5
		));
		?>
		<?php foreach($recent_articles as $recent_article): ?>
			<li><a href="<?php echo get_permalink($recent_article->ID); ?>"><?php echo $recent_article->post_title; ?></a></li>
		<?php endforeach; ?>
	</ul>
	<?php endif; ?>

	<ul class="sidebar-menu sidebar-menu-tags">
		<?php
			$post_tags = get_terms( 'post_tag', array('orderby' => 'count', 'order' => 'DESC'));
		?>
		<li>Tags</li>
		<?php foreach($post_tags as $post_tag): ?>
			<li><a href="<?php echo get_term_link( $post_tag, 'post_tag'); ?>"><?php echo $post_tag->name; ?></a></li>
		<?php endforeach; ?>
	</ul>

	<ul class="sidebar-menu">
		<li>Archives</li>
		<?php wp_get_archives(); ?>
	</ul>

	
</div><!-- .sidebar -->