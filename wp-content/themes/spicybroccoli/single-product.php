<?php
/**
 * The main template file.
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Twenty_Ten
 * @since Twenty Ten 1.0
 */

get_header(); ?>

	<style>

	.fixed-scroll .SiteHeader__page {
		height: 122px;
		position: fixed;
		z-index: 1;
	}
	.fixed-scroll .panel-header {
		height: 185px;
		top: 122px;
		position: fixed;
		z-index: 1;
		overflow: hidden;
	}
	.fixed-scroll .panel-grey {
		top: 307px;

	}
	.fixed-scroll .sidebar {
		position: fixed;
		top: 307px;
	}
	.fixed-scroll #footer {
		margin-top: 307px;
	}
	</style>

		<section class="panel panel-header">
		<img src="<?php echo get_stylesheet_directory_uri(); ?>/images/header_img.jpg">
	</section><!-- .panel -->

	<?php if (have_posts()) while(have_posts()): the_post(); ?>
	<section class="panel panel-grey">
		<div class="wrapper">
			<div class="sidebar">
				<ul id="navbar" class="sidebar-menu sidebar-menu-product">
					<li data-section="overview" href="#overview" class="active">Overview</li>
					<?php if (get_post_meta(get_the_ID(), '_features', true)): ?>
						<li href="#features" data-section="features">Features</li>
					<?php endif; ?>
					<?php if (get_post_meta(get_the_ID(), '_case_study_text',  true ) && get_post_meta(get_the_ID(), '_case_study_link',  true ) ): ?>
						<li href="#case-study" data-section="case-study">Case Study</li>
					<?php endif; ?>
					<li href="#enquiry" data-section="enquiry">Make an Enquiry</li>
				</ul>
				<ul class="sidebar-menu sidebar-menu-associated-products">
					<?php $_post = get_queried_object(); ?>
					<?php $connected = new WP_Query( array(
					  'connected_type' => 'products_to_products',
					  'connected_items' => get_queried_object(),
					  'nopaging' => true,
					  'orderby' => 'menu_order',
					  'order' => 'ASC'
					) );

					// Display connected pages
					if ( $connected->have_posts() ) : ?>
						<li>Associated Products</li>
						<?php while ( $connected->have_posts() ) : $connected->the_post(); ?>
							<li><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></li>
						<?php endwhile; ?>
					<?php endif; ?>
					<?php wp_reset_query(); 
					wp_reset_postdata(); ?>
				</ul>
				<ul class="sidebar-menu sidebar-menu-other-products">
				<?php 
				$exclude = array_map(function($item){
					return $item->ID;
				}, $connected->posts);

				// Also exclude current post
				$exclude[] = $_post->ID;
				?>
				<?php
				$other_products = new WP_Query(array(
					'post__not_in' => $exclude,
					'post_type' => 'product',
					'posts_per_page' => -1,
					'orderby' => 'menu_order',
					'order' => 'ASC'
				));
				?>
					<?php if ($other_products->have_posts()): ?>
						<li>Other Products</li>
						<?php while($other_products->have_posts()): $other_products->the_post(); ?>
							<li><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></li>
						<?php endwhile; ?>
					<?php endif; ?>
					<?php wp_reset_query(); 
					wp_reset_postdata(); ?>
				</ul>
			</div><!-- .sidebar -->

			<div class="content">
				<div class="content-wrapper">
					<div class="Product">
						<div id="overview" class="Product--overview Product--section">
							<div class="Product--title">
								<?php 
								if ( has_post_thumbnail() ) { // check if the post has a Post Thumbnail assigned to it.
									the_post_thumbnail('thumbnail', array( 'class' => 'product--logo' ) );
								} 
								?>

								<h1><?php the_title(); ?></h1>
							</div>

							<div class="Product--headline">
								<h2><?php echo get_post_meta(get_the_ID(), '_headline', true); ?></h2>
							</div>

							<div class="Product--hero">
								<?php echo wpautop(get_post_meta(get_the_ID(), '_hero', true)); ?>
							</div>

						</div><!-- .Product--overview -->

						<?php $features = get_post_meta(get_the_ID(), '_features', true); ?>
						<?php if ($features): ?>
							<div id="features" class="Product--features Product--section">
								<h3>Features</h3>
									<?php for ($i = 1; $i <= count($features); $i++): ?>
										<div class="Product--features-item cf">
											<img src="<?php echo wp_get_attachment_url($features[$i]['image']); ?>" />
											<div class="Product--features-item-text"><?php echo $features[$i]['text']; ?></div>
										</div>
									<?php endfor; ?>		
							</div><!-- .Product--features -->
						<?php endif; ?>
                        
                        <?php if ( get_post_meta(get_the_ID(), '_resources',  true )): ?>
							<div class="Product--resources Product--section">
                            <h3>Resources</h3>
								<?php echo get_post_meta(get_the_ID(), '_resources',  true ); ?>
							</div><!-- .Product--resources -->
						<?php endif; ?>
                        

						<?php if (get_post_meta(get_the_ID(), '_case_study_text',  true ) && get_post_meta(get_the_ID(), '_case_study_link',  true ) ): ?>
							<div id="case-study" class="Product--casestudy Product--section">
								<h3>Case Study</h3>
								<p><?php echo get_post_meta(get_the_ID(), '_case_study_text',  true ); ?></p>
								<?php echo get_post_meta(get_the_ID(), '_case_study_link',  true ); ?>
							</div><!-- .Product--casestudy -->
						<?php endif; ?>

						<div id="enquiry" class="Product--contact Product--section">
							<h4>Get in Touch</h4>
							<p>Have a question or just want to get in touch?<br />Drop us a line and we will get back to you shortly with any enquiries.</p>
							<?php echo do_shortcode("[contact-form-7 id=116 title=contact-form]"); ?>
						</div><!-- .Product--contact -->

						<?php if ( get_post_meta(get_the_ID(), '_testimonial',  true )): ?>
							<div class="Product--testimonial Product--section">
								<?php echo get_post_meta(get_the_ID(), '_testimonial',  true ); ?>
							</div>
						<?php endif; ?>

					</div><!-- .Product -->
					
				</div><!-- .content-wrapper -->
			</div><!-- .content -->
		</div>
	</section><!-- .panel -->
<?php endwhile; ?>

<?php get_footer(); ?>