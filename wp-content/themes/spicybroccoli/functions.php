<?php
/**
 * Theme Functions
 */

/**
 * Set the content width based on the theme's design and stylesheet.
 * Used to set the width of images and content. Should be equal to the width the theme
 * is designed for, generally via the style.css stylesheet.
 */
if ( ! isset( $content_width ) )
	$content_width = 640;

/** Tell WordPress to run twentyten_setup() when the 'after_setup_theme' hook is run. */
add_action( 'after_setup_theme', function() {

	// This theme uses post thumbnails
	add_theme_support( 'post-thumbnails' );
	add_theme_support( 'html5', array( 'comment-list', 'comment-form', 'search-form', 'gallery', 'caption' ) );

	// This theme uses wp_nav_menu() in one location.
	register_nav_menus( array(
			'primary' => 'Primary Navigation',
			'footer' => 'Footer Navigation',
		) );

});

// Bootstrap Theme
include('inc/template-utils.php'); // Load Template Utilities
include('inc/branding.php'); // Load Template Utilities
include('inc/responsive-nav.php'); // Responsive Nav
include('inc/routes.php');
// include('inc/landing.php'); // Load Landing page if necessary
include('inc/scripts.php'); // Load Scripts
include('inc/cpt-product.php');
include('inc/cpt-testimonial.php');

include('inc/customize/theme-customizer-demo.php');


function modify_contact_methods($profile_fields) {

	// Add new fields
	$profile_fields['linkedin'] = 'LinkedIn';

	return $profile_fields;
}
add_filter('user_contactmethods', 'modify_contact_methods');

function better_wpautop($pee){
return wpautop($pee,false);
}



remove_filter( 'the_content', 'wpautop' );
add_filter( 'the_content', 'better_wpautop' , 99);
add_filter( 'the_content', 'shortcode_unautop',100 );


function my_connection_types() {
    p2p_register_connection_type( array(
        'name' => 'products_to_products',
        'from' => 'product',
        'to' => 'product',
        'title' => 'Associated Products'
    ) );

    p2p_register_connection_type( array(
        'name' => 'pages_to_products',
        'from' => 'page',
        'to' => 'product',
    ) );
}
add_action( 'p2p_init', 'my_connection_types' );


 add_action( 'post_submitbox_misc_actions', 'article_or_box' );
add_action( 'save_post', 'save_article_or_box' );
function article_or_box() {
    global $post;
    if (get_post_type($post) == 'product' || get_post_type($post) == 'post') {
        echo '<div class="misc-pub-section misc-pub-section-last" style="border-top: 1px solid #eee;">';

     
        $val =  ( get_post_meta( $post->ID, 'featured', true ) == 0 || get_post_meta( $post->ID, 'featured', true ) == '') ? 0: 1;
     


        echo '<input type="checkbox" name="featured" id="featured" value="1"  '.checked($val,'1',false).' /> <label for="featured" class="select-it">Featured</label><br />';
        echo '</div>';
       }
  
}
function save_article_or_box($post_id) {

    if (!isset($_POST['post_type']) )
        return $post_id;

    if ($_POST['post_type'] == 'product' || $_POST['post_type'] == 'post') {
        
	    if ( defined('DOING_AUTOSAVE') && DOING_AUTOSAVE ) 
	        return $post_id;


	    if ( defined('DOING_AJAX') && DOING_AJAX ) 
	        return $post_id;
	    
	     
	    update_post_meta( $post_id, 'featured', $_POST['featured'] );
 
    } else {
    	return $post_id;
    } 

}

function sbm_smart_excerpt() {
	global $post;
	if ($post->post_excerpt) {
		echo wpautop($post->post_excerpt);
	} else {
		$string = strip_tags($post->post_content);
		$truncation = 25;
		$matches = preg_split("/\s+/", $string);
		$count = count($matches);

		if($count > $truncation) {
		    //Grab the last word; we need to determine if
		    //it is the end of the sentence or not
		    $last_word = strip_tags($matches[$truncation-1]);
		    $lw_count = strlen($last_word);

		    //The last word in our truncation has a sentence ender
		    if($last_word[$lw_count-1] == "." || $last_word[$lw_count-1] == "?" || $last_word[$lw_count-1] == "!") {
		        for($i=$truncation;$i<$count;$i++) {
		            unset($matches[$i]);
		        }

		    //The last word in our truncation doesn't have a sentence ender, find the next one
		    } else {
		        //Check each word following the last word until
		        //we determine a sentence's ending
		        for($i=($truncation);$i<$count;$i++) {
		            if($ending_found != TRUE) {
		                $len = strlen(strip_tags($matches[$i]));
		                if($matches[$i][$len-1] == "." || $matches[$i][$len-1] == "?" || $matches[$i][$len-1] == "!") {
		                    //Test to see if the next word starts with a capital
		                    if($matches[$i+1][0] == strtoupper($matches[$i+1][0])) {
		                        $ending_found = TRUE;
		                    }
		                }
		            } else {
		                unset($matches[$i]);
		            }
		        }
		    }

		    //Check to make sure we still have a closing <p> tag at the end
		    $body = implode(' ', $matches);
		    if(substr($body, -4) != "</p>") {
		        $body = $body."</p>";
		    }

		    echo wpautop($body); 
		} else {
		    echo wpautop($string);
		}
	}
}


class SBM_Post_Metabox {

    public function __construct() {
            add_action( 'add_meta_boxes', array( $this, 'add_meta_box' ) );
            add_action( 'save_post', array( $this, 'save' ), 10 , 1 );
       
    }

    public function add_meta_box( $post_type ) {
    	global $post;
    	if ($post->ID == 26) {
        	add_meta_box( 'pagemetadiv', 'Homepage Pull Quote', array( $this, 'render_meta_box_content' ), 'page', 'advanced', 'high');
    	}
    }

    public function save( $post_id ) {
    
        /*
         * We need to verify this came from the our screen and with proper authorization,
         * because save_post can be triggered at other times.
         */

    

        // If this is an autosave, our form has not been submitted,
                //     so we don't want to do anything.
        if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) 
            return $post_id;

        // Check the user's permissions.
        if ( 'page' == $_POST['post_type'] ) {

            if ( ! current_user_can( 'edit_page', $post_id ) )
                return $post_id;
    
        } else {

            if ( ! current_user_can( 'edit_post', $post_id ) )
                return $post_id;
        }

        /* OK, its safe for us to save the data now. */

        // Sanitize the user input.



        // Update the meta field.
        $res = update_post_meta( $post_id, '_home_page_pull_quote', $_POST['home_page_pull_quote'] );


    

    }

    public function render_meta_box_content( $post ) {

        // Enqueue media scripts
        //wp_enqueue_scripts();
    
        // Add an nonce field so we can check for it later.
   

        // Display the form, using the current value.
        $this->textarea_field(array('key' => 'home_page_pull_quote', 'heading' => 'Homepage Pull Quote'));
    }

    private function textarea_field ( $opts ) { ?>
      <?php 

      global $post;
      $key = sanitize_title($opts['key']); 
      $value = ( get_post_meta($post->ID, '_' . $key, true) ) ? get_post_meta($post->ID, '_' . $key, true) : ''; 

      var_dump($value);

      ?>

      <div class="row row-<?php echo $key; ?>">

        <?php wp_editor($value, $key); ?>
      </div>

    <?php 
      // Reset $value
      $value = null;

    }

}
new SBM_Post_Metabox();