/*
 * Spicy Broccoli Utilities - site helper functions
 * from bonzo https://github.com/ded/bonzo
 *
 */
(function(window) {
    'use strict';
    var cache = {};
    var requestAnimFrame = (function() {
        return window.requestAnimationFrame || window.webkitRequestAnimationFrame || window.mozRequestAnimationFrame || function(callback) {
            window.setTimeout(callback, 1000 / 60);
        };
    })();

    function extend(a, b) {
        for (var key in b) {
            if (b.hasOwnProperty(key)) {
                a[key] = b[key];
            }
        }
        return a;
    }

    function hasParent(e, id) {
        if (!e) return false;
        var el = e.target || e.srcElement || e || false;
        while (el && el.id != id) {
            el = el.parentNode || false;
        }
        return (el !== false);
    }

    function getLevelDepth(e, id, waypoint, cnt) {
        cnt = cnt || 0;
        if (e.id.indexOf(id) >= 0) return cnt;
        if (el.classList.contains(waypoint)) {
            ++cnt;
        }
        return e.parentNode && getLevelDepth(e.parentNode, id, waypoint, cnt);
    }

    function mobileCheck() {
        var check = false;
        (function(a) {
            if (/(android|ipad|playbook|silk|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows (ce|phone)|xda|xiino/i.test(a) || /1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(a.substr(0, 4))) check = true
        })(navigator.userAgent || navigator.vendor || window.opera);
        return check;
    }

    function closest(e, classname) {
        if (e.classList.contains(classname)) {
            return e;
        }
        return e.parentNode && closest(e.parentNode, classname);
    }

    function tmpl(str, data) {
        var fn = !/\W/.test(str) ? cache[str] = cache[str] || tmpl(document.getElementById(str).innerHTML) : new Function("var p=[];" + "p.push('" +
            str.replace(/[\r\t\n]/g, " ").split("<%").join("\t").replace(/((^|%>)[^\t]*)'/g, "$1\r").replace(/\t=(.*?)%>/g, "',$1,'").split("\t").join("');").split("%>").join("p.push('").split("\r").join("\\'") + "');return p.join('');");
        return data ? fn(data) : fn;
    }

    function easeInOutQuad(t, b, c, d) {
        t /= d / 2;
        if (t < 1) {
            return c / 2 * t * t + b;
        }
        t--;
        return -c / 2 * (t * (t - 2) - 1) + b;
    }

    function easeInCubic(t, b, c, d) {
        var tc = (t /= d) * t * t;
        return b + c * (tc);
    }

    function inOutQuintic(t, b, c, d) {
        var ts = (t /= d) * t,
            tc = ts * t;
        return b + c * (6 * tc * ts + -15 * ts * ts + 10 * tc);
    }

    function scrollTo(to, callback, duration) {
        var doc = (navigator.userAgent.indexOf('Firefox') != -1 || navigator.userAgent.indexOf('MSIE') != -1 || navigator.userAgent.indexOf('Trident') != -1) ? document.documentElement : document.body,
            start = doc.scrollTop,
            change = to - start,
            currentTime = 0,
            increment = 20;
        duration = (typeof(duration) === 'undefined') ? 500 : duration;
        console.log(to);
        var animateScroll = function() {
            currentTime += increment;
            var val = easeInOutQuad(currentTime, start, change, duration);
            doc.scrollTop = val;
            if (currentTime < duration) {
                requestAnimFrame(animateScroll);
            } else {
                if (callback && typeof(callback) === 'function') {
                    callback();
                }
            }
        };
        animateScroll();
    }
    var utils = {
        extend: extend,
        hasParent: hasParent,
        getLevelDepth: getLevelDepth,
        isMobile: mobileCheck,
        closest: closest,
        template: tmpl,
        scrollTo: scrollTo
    };
    if (typeof define === 'function' && define.amd) {
        define(utils);
    } else {
        window.utils = utils;
    }
})(window);

(function(window, document) {
	"use strict";

var spicybroccoli = {
	vars: {
		isFixedScrolling: true,
		organicScroll: true
	}
};

spicybroccoli.init = function() {
	// Setup site variables
	spicybroccoli.vars.docBody = (navigator.userAgent.indexOf('Firefox') != -1 || navigator.userAgent.indexOf('MSIE') != -1 || navigator.userAgent.indexOf('Trident') != -1) ? document.documentElement : document.body;
	spicybroccoli.vars.docHeight = window.innerHeight || spocybroccoli.vars.docBody.clientHeight;
	spicybroccoli.vars.isFixedScrolling = spicybroccoli.detectIfFixedScrolling();
	spicybroccoli.vars.windowHeight = jQuery(window).height();
    spicybroccoli.vars.sidebarFixed = false;
    spicybroccoli.vars.sidebarsHidden = false;
    

    if (spicybroccoli.vars.isFixedScrolling) {
        // jQuery('body').addClass('fixed-scroll')
    }

   jQuery(window).on('load', function(){
    spicybroccoli.vars.sidebar = jQuery('.sidebar-menu-product').length && jQuery('.sidebar-menu-product').offset().top;
   })
	console.log('Site Vars: ', spicybroccoli.vars);
    spicybroccoli.productSidebarLinks();
    spicybroccoli.onPageScroll();
    spicybroccoli.homeVideo();

}

spicybroccoli.productSidebarLinks = function() {
	jQuery('.sidebar-menu-product li').on('click',function(e){

			spicybroccoli.scrollPage(jQuery(this).data('section'));
		});
};

spicybroccoli.detectIfFixedScrolling = function() {
	var footerHeight = jQuery('#footer').height() + jQuery('#colophon').height();
	var remainingHeight = jQuery(window).height() - jQuery('#header').height() - jQuery('.panel-header').height() - jQuery('.sidebar').height();
	console.log('Remaining Height:', remainingHeight);
	console.log('Footer Height: ', footerHeight);
	return (remainingHeight >= footerHeight);
}

spicybroccoli.singleProductScroll = function() {
		if (!spicybroccoli.vars.isFixedScrolling) {

                console.log(jQuery(document).scrollTop());

                if ( ( jQuery(document).scrollTop() > ( spicybroccoli.vars.sidebar - 100 ) ) && jQuery(document).scrollTop() <= spicybroccoli.vars.sidebar ) {
                    jQuery('.sidebar-menu-associated-products').fadeOut();
                    jQuery('.sidebar-menu-other-products').fadeOut();

                } else if (jQuery(document).scrollTop() > ( spicybroccoli.vars.sidebar) ) {
  
					jQuery('.sidebar-menu-product').css('position', 'fixed').css('top', 0).css('width', 256);
                   
                    
				} else if (jQuery(document).scrollTop() <= spicybroccoli.vars.sidebar) { 
				    jQuery('.sidebar-menu-associated-products').fadeIn();
                    jQuery('.sidebar-menu-other-products').fadeIn();

                    jQuery('.sidebar-menu-product').removeAttr('style');
				}
			}
		

			if (spicybroccoli.vars.organicScroll) {

               
				
 				var scroll = jQuery(document).scrollTop();
	           	var $sections = jQuery('.Product--section');
	     

	           	var selectedEl;
	           $sections.each(function(index){
	    
	           	if (scroll <= jQuery(this).offset().top && index == 0) {
	           		selectedEl = this;
	           	} else if (scroll >= jQuery(this).offset().top && scroll <= jQuery($sections[index+1]).offset().top) {
	           		selectedEl = this;
	           	}
	           });

	           jQuery('.sidebar-menu-product li.active').removeClass('active')
	           jQuery('.sidebar-menu-product li[data-section="' + selectedEl.id + '"]').addClass('active')

			}
}

spicybroccoli.onPageScroll = function() {
		jQuery(window).on('scroll', function(e){

				if (!!jQuery('.single-product').length) {
					spicybroccoli.singleProductScroll();
				}

	
		});
}

spicybroccoli.scrollPage = function(target, duration) {
	var duration = duration || 2000;
	var scrollPoint = (spicybroccoli.vars.isFixedScrolling) ? jQuery('#'+target).offset().top - 307 : jQuery('#'+target).offset().top +5;
	console.log(scrollPoint);
		spicybroccoli.vars.organicScroll = true;
	utils.scrollTo(scrollPoint, function(){
//
	//	spicybroccoli.vars.organicScroll = true;
	}, duration);
};

spicybroccoli.homeVideo = function() {
      jQuery("video").each(function (i, v) {
        (function(i, v){
            var video = jQuery(v).get(0);
            if (jQuery(v)) {
                jQuery(v).closest("div").hover(function () {
                    v.play();
                }, function () {
                   v.pause();
                });
            }
            video.addEventListener("ended", video.play);
        })(i, v);

    });
};


// Run it
jQuery(spicybroccoli.init);

}(window, document));