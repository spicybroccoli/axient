<?php
//include('lib/hm-rewrite.php');


	add_action( 'init', 'sbm_rewrite', 99 );

	function sbm_rewrite() {


    	add_rewrite_rule('^blog(?:\/page\/([0-9]+))?$', 'index.php?post_type=post&blog=archive&paged=$matches[1]', 'top');
    	add_rewrite_rule('^blog\/tag\/([a-zA-z\-]+)(?:\/page\/([0-9]+))?$', 'index.php?post_type=post&tag=$matches[1]&paged=$matches[2]&blog=archive', 'top');
    	add_rewrite_rule('^blog\/([0-9]{4})(?:\/page\/([0-9]+))?$', 'index.php?post_type=post&year=$matches[1]&paged=$matches[2]&blog=archive', 'top');
    	add_rewrite_rule('^blog\/([0-9]{4})\/([0-9]{1,2})(?:\/page\/([0-9]+))?$', 'index.php?post_type=post&year=$matches[1]&month=$matches[2]&paged=$matches[3]&blog=archive', 'top');
    	add_rewrite_rule('^blog\/authors$', 'index.php?blog=author-archive', 'top');
	}


	function sbm_query_vars( $query_vars ){


    	$query_vars[] = 'blog';

    	return $query_vars;
	}

	add_filter( 'query_vars', 'sbm_query_vars');

function wpd_parse_request(){
	global $wp_query;
	if (is_admin()) {
		return;
	}

	if ($wp_query->query_vars['blog'] == 'archive' ) {
		$wp_query->is_home = false;
		$wp_query->is_404 = false;
		$wp_query->is_archive = true;
	
	} else if ($wp_query->query_vars['blog'] == 'author-archive' ) {
		$wp_query->is_home = false;
		$wp_query->is_404 = false;
		$wp_query->is_archive = true;
	}
}
add_action( 'template_redirect', 'wpd_parse_request', 1 );

add_filter('template_include', function($template){
	global $wp_query;
	if ($wp_query->query_vars['blog'] == 'author-archive') {
		$template = locate_template('archive-authors.php');
	}
	return $template;
}, 10, 1);

add_filter('body_class', function($classes){
	global $wp_query;
	if ($wp_query->query_vars['blog'] == 'author-archive') {
		$classes[] = 'author-archive';
	}
	return $classes;
});

add_filter('wp_title', 'filter_pagetitle');
function filter_pagetitle($title) {
    //check if its a blog post
   	global $wp_query;
	if ($wp_query->query_vars['blog'] == 'author-archive') {
		$title = 'Author Archive | ' . $title;
	}
    //if wordpress can't find the title return the default
    return $title;
}
