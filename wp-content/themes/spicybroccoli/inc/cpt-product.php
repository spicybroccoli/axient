<?php
add_action( 'init', 'register_cpt_product' );

function register_cpt_product() {

    $labels = array( 
        'name' => _x( 'Products', 'product' ),
        'singular_name' => _x( 'Product', 'product' ),
        'add_new' => _x( 'Add New', 'product' ),
        'add_new_item' => _x( 'Add New Product', 'product' ),
        'edit_item' => _x( 'Edit Product', 'product' ),
        'new_item' => _x( 'New Product', 'product' ),
        'view_item' => _x( 'View Product', 'product' ),
        'search_items' => _x( 'Search Products', 'product' ),
        'not_found' => _x( 'No products found', 'product' ),
        'not_found_in_trash' => _x( 'No products found in Trash', 'product' ),
        'parent_item_colon' => _x( 'Parent Product:', 'product' ),
        'menu_name' => _x( 'Products', 'product' ),
    );

    $args = array( 
        'labels' => $labels,
        'hierarchical' => false,
        
        'supports' => array( 'title', 'editor', 'thumbnail', 'page-attributes' ),
        
        'public' => true,
        'show_ui' => true,
        'show_in_menu' => true,
        
        
        'show_in_nav_menus' => true,
        'publicly_queryable' => true,
        'exclude_from_search' => false,
        'has_archive' => true,
        'query_var' => true,
        'can_export' => true,
        'rewrite' => array('with_front' => false, 'slug' => 'product'),
        'capability_type' => 'post'
    );

    register_post_type( 'product', $args );
}

class SBM_Product_Metabox {

    public function __construct() {
            add_action( 'add_meta_boxes', array( $this, 'add_meta_box' ) );
            add_action( 'save_post', array( $this, 'save' ) );
       
        }

        /**
         * Adds the meta box container.
         */
        public function add_meta_box( $post_type ) {
            add_meta_box( 'productoverviewdiv', 'Overview', array( $this, 'render_meta_box_content_overview' ), 'product', 'advanced', 'high');

            add_meta_box( 'productfeaturesdiv', 'Features', array( $this, 'render_meta_box_content_features' ), 'product', 'advanced', 'high');

            add_meta_box( 'productcasestudydiv', 'Case Study', array( $this, 'render_meta_box_content_case_study' ), 'product', 'advanced', 'high');

            add_meta_box( 'producttestimonialsdiv', 'Testimonials', array( $this, 'render_meta_box_content_testimonials' ), 'product', 'advanced', 'high');

            add_meta_box( 'productresourcesdiv', 'Resources', array( $this, 'render_meta_box_content_resources' ), 'product', 'advanced', 'high');

            add_meta_box( 'producthomediv', 'Home Page Promo Details', array( $this, 'render_meta_box_home' ), 'product', 'advanced', 'high');
                
        }

        /**
         * Save the meta when the post is saved.
         *
         * @param int $post_id The ID of the post being saved.
         */
        public function save( $post_id ) {
        
            /*
             * We need to verify this came from the our screen and with proper authorization,
             * because save_post can be triggered at other times.
             */

            // Check if our nonce is set.
            if ( ! isset( $_POST['sbm_product_nonce'] ) )
                return $post_id;

            $nonce = $_POST['sbm_product_nonce'];

            // Verify that the nonce is valid.
            if ( ! wp_verify_nonce( $nonce, 'sbm_product' ) )
                return $post_id;

            // If this is an autosave, our form has not been submitted,
                    //     so we don't want to do anything.
            if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) 
                return $post_id;

            // Check the user's permissions.
            if ( 'product' == $_POST['post_type'] ) {

                if ( ! current_user_can( 'edit_page', $post_id ) )
                    return $post_id;
        
            } else {

                if ( ! current_user_can( 'edit_post', $post_id ) )
                    return $post_id;
            }

            /* OK, its safe for us to save the data now. */

            // Sanitize the user input.



            // Update the meta field.
            update_post_meta( $post_id, '_headline', sanitize_text_field( $_POST['headline']) );
            update_post_meta( $post_id, '_hero',  $_POST['hero'] );
            update_post_meta( $post_id, '_features',  $_POST['features'] );
            update_post_meta( $post_id, '_case_study_text',  $_POST['case_study_text'] );
            update_post_meta( $post_id, '_case_study_link',  $_POST['case_study_link'] );
            update_post_meta( $post_id, '_testimonial',  $_POST['testimonial'] );
            update_post_meta( $post_id, '_resources',  $_POST['resources'] );


            update_post_meta( $post_id, '_promo_title',  $_POST['promo_title'] );
            update_post_meta( $post_id, '_promo_video_overlay',  $_POST['promo_video_overlay'] );
            update_post_meta( $post_id, '_video_h264',  $_POST['video_h264'] );
            update_post_meta( $post_id, '_video_vp8',  $_POST['video_vp8'] );
            update_post_meta( $post_id, '_promo_desc',  $_POST['promo_desc'] );
            update_post_meta( $post_id, '_cta_label',  $_POST['cta_label'] );
            update_post_meta( $post_id, '_home_order',  $_POST['home_order'] );
            update_post_meta( $post_id, '_video_thumb',  $_POST['video_thumb'] );


      

        }


        /**
         * Render Meta Box content.
         *
         * @param WP_Post $post The post object.
         */
        public function render_meta_box_content_overview( $post ) {

            // Enqueue media scripts
            //wp_enqueue_scripts();
        
            // Add an nonce field so we can check for it later.
            wp_nonce_field( 'sbm_product', 'sbm_product_nonce' );

            // Display the form, using the current value.
            $this->textarea_field(array('key' => 'headline', 'heading' => 'Headline'));
            $this->textarea_field(array('key' => 'hero', 'heading' => 'hero'));

            
        
            








            
       }

       public function render_meta_box_content_features( $post ) {
          $this->features_field(array('key' => 'hero', 'heading' => 'hero'));
       }

       public function render_meta_box_content_case_study() {
          $this->textarea_field(array('key' => 'case_study_text', 'heading' => 'Case Study Text'));
          $this->textarea_field(array('key' => 'case_study_Link', 'heading' => 'Case Study Link'));
       }

       public function render_meta_box_content_testimonials() {
        $this->textarea_field(array('key' => 'testimonial', 'heading' => 'Testimonial'));
       }

       public function render_meta_box_content_resources() {
        $this->textarea_field(array('key' => 'resources', 'heading' => 'Resources'));
       }

       public function render_meta_box_home() {
          $this->textarea_field(array('key' => 'promo_title', 'heading' => 'Promo Title'));
          $this->textarea_field(array('key' => 'promo_video_overlay', 'heading' => 'Promo Video Overlay'));
          $this->upload_field(array('key' => 'video_h264', 'heading' => 'Video - MP4 (H.264)'));
          $this->upload_field(array('key' => 'video_vp8', 'heading' => 'Video - WebM (VP8)'));
          $this->upload_field(array('key' => 'video_thumb', 'heading' => 'Video Thumbnail'));
          $this->textarea_field(array('key' => 'promo_desc', 'heading' => 'Description'));
          $this->input_field(array('key' => 'cta_label', 'heading' => 'CTA Label'));
           $this->input_field(array('key' => 'home_order', 'heading' => 'Home Order'));
       }

       private function new_wp_editor($content, $id) {

       }

       private function features_field( $opts ) { ?>



          <?php  
          global $post;
          wp_enqueue_media();

          $key = 'features';
        $value = ( get_post_meta($post->ID, '_' . $key, true) ) ? get_post_meta($post->ID, '_' . $key, true) : '';

                  $thumbnail = includes_url('/images/media/document.png');

        
        
         ?>
            
           <div class="row row-features">
             
            <ul>
            <?php if (!empty($value) && count($value) > 0): ?>
            <?php for($i = 1; $i <= count($value); $i++): ?>
             <li class="row" data-id="<?php echo $i; ?>">
              <div class="remove-feature">&times;</div>
              <div id="media-items" class="hide-if-no-js">
                <div id="media-item" class="media-item child-of-0 open">
                  <img class="pinkynail" src="<?php echo $thumbnail; ?>" alt="">
                  <button class="button edit-attachment select">Select Image</button>
                  <input type="hidden" name="features[<?php echo $i; ?>][image]" value="<?php echo $value[$i]['image']; ?>" />
                  <?php 
                  $filename = ( get_attached_file($value[$i]['image']) ) 
                    ? basename(get_attached_file($value[$i]['image'])) 
                    : 'Nothing Selected'; 
                  ?>
                  <div class="filename new"><span class="title"><strong><?php echo $filename; ?></strong></span></div>
                </div>
              </div>
              <textarea type="text" name="features[<?php echo $i; ?>][text]"><?php echo $value[$i]['text']; ?></textarea>
            </li>
            <?php endfor; ?>
          <?php endif; ?>
            </ul>


                    <?php if (!empty($value) && count($value) > 0) { 
                        for($i = 0; $i < count($value['text']); $i++) { ?>
                       <div class="single-feature">
                            
                            <input type="text" value="<?php echo $value['image'][$i]; ?>" name="features[image][]" />
                            <textarea type="text" name="features[text][]"><?php echo $value['text'][$i]; ?></textarea>
                            <div class="remove-feature">&times;</div>
                            </div>
                        <?php }
                    } ?>            
           
           </div>
           <style>
           .media-item .edit-attachment {
             display: block;
             line-height: 28px;
             float: right;
             margin-right: 60px;
           }
           .remove-feature {
             position: absolute;
             right: 20px;
             font-size: 23px;
             top: 5px;
             cursor: pointer;
           }
           .row-features li {
            position: relative;
            margin-bottom: 20px;
           }
           .row-features textarea { 
              width: 100%;
              height: 200px;
              margin-top: 10px;
              padding: 10px !important;
              display: block;
           }
           </style>
           <script>
             jQuery(function(){
               (function() {
                   jQuery('.row-features').on('click', '.select', function(e){
                       window.self = this;
                       e.preventDefault();
                  
                       // If the media frame already exists, reopen it.
                       if ( modal ) {
                         modal.open();
                         return;
                       }
                   });

                  var modal = wp.media({
                       title: 'Choose <?php echo $opts["label"]; ?>',
                       frame: 'select',
                       button: {
                                 text: 'Select <?php echo $opts["label"]; ?>'
                             },
                             multiple: false,
                   });

                   modal.on( 'select', function() {
                     // We set multiple to false so only get one image from the uploader
                     attachment = modal.state().get('selection').first().toJSON();
                     console.log(jQuery(window.self).parents('#media-item').find('input'));
                     jQuery(window.self).parents('#media-item').find('input').val(attachment.id);
                     jQuery(window.self).parents('#media-item').find('span.title strong').text(attachment.filename);
                    // jQuery('#media-item-<?php echo $key; ?> .pinkynail').attr('src', attachment.url)
                   });

                })();
             }); 
           </script>
           <button class="add-features button">Add Feature</button>

           <script>
           // Math.max.apply(null, jQuery('[data-count]').map(function() { return jQuery(this).data('count') } ))
           jQuery(document).ready(function(){
                var repeatTemplate = wp.template( "features-repeat" );
                jQuery('.row-features').on('click', '.remove-feature', function(e){
                    var parent = jQuery(this).parents('li').remove();
                })
                jQuery('.add-features').on('click', function(e){
                 
                    e.preventDefault(); 
                    var ids = jQuery('.row-features [data-id]').map(function() { return jQuery(this).data('id') } )
                    var id = (!!ids.length) ? Math.max.apply(null, ids) : 0;
                    id++;
                    var html = repeatTemplate({id: id, thumbnail: '<?php echo $thumbnail; ?>' })
                    jQuery('.row-features ul').append(html);
                })
           })
           </script>

           <script  type="text/html" id="tmpl-features-repeat">
             <li class="row" data-id="{{data.id}}">
             <div class="remove-feature">&times;</div>
               <div id="media-items" class="hide-if-no-js">
                 <div id="media-item" class="media-item child-of-0 open">
                   <img class="pinkynail" src="{{data.thumbnail}}" alt="">
                   <button class="button edit-attachment select">Select Image</button>
                   <input type="hidden" name="features[{{data.id}}][image]" value="<?php echo $attach_id; ?>" />
                   <div class="filename new"><span class="title"><strong>Nothing Selected</strong></span></div>
                 </div>
               </div>
               <textarea type="text" name="features[{{data.id}}][text]"></textarea>
             </li>
           </script>

      <?php }



      private function input_field( $opts ) { ?>
        <?php 

        global $post;
        $key = sanitize_title($opts['key']); 
        $value = ( get_post_meta($post->ID, '_' . $key, true) ) ? get_post_meta($post->ID, '_' . $key, true) : ''; 

        ?>

        <div class="row row-<?php echo $key; ?>">
          <label for="<?php echo $key; ?>">
            <?php echo $opts['heading']; ?>
          </label>
          <input type="text" id="<?php echo $key; ?>" name="<?php echo $key; ?>" value=" <?php echo esc_attr( trim($value) ); ?>" size="25" />
        </div>

      <?php
        // Reset $value
        $value = null;
      }

      private function textarea_field ( $opts ) { ?>
        <?php 

        global $post;
        $key = sanitize_title($opts['key']); 
        $value = ( get_post_meta($post->ID, '_' . $key, true) ) ? get_post_meta($post->ID, '_' . $key, true) : ''; 

        ?>

        <div class="row row-<?php echo $key; ?>">
          <label for="<?php echo $key; ?>">
            <?php echo $opts['heading']; ?>
          </label>
          <?php wp_editor($value, $key); ?>
        </div>

      <?php 
        // Reset $value
        $value = null;

      }

     private function upload_field( $opts ) {
        global $post;
        wp_enqueue_media(); 

        $defaults = array(
            'heading' => 'Image',
            'key' => 'image',
            'size' => 'thumbnail'
        );
        $opts = wp_parse_args( $opts, $defaults );
        $key = sanitize_title($opts['key']);
        $attach_id = ( get_post_meta($post->ID, '_' . $key, true) ) ? get_post_meta($post->ID, '_' . $key, true) : ''; 



        $thumbnail = includes_url('/images/media/document.png');

        $filename = (get_attached_file($attach_id)) ? basename(get_attached_file($attach_id)) : 'Nothing Selected';

        ?>

        <div class="row row-<?php echo $key; ?>">
        <label for="<?php echo $key; ?>"><?php echo $opts['heading']; ?></label>
          <div id="media-items" class="hide-if-no-js">
            <div id="media-item-<?php echo $key; ?>" class="media-item child-of-0 open">
              <img class="pinkynail" src="<?php echo $thumbnail; ?>" alt="">
              <button class="button edit-attachment select-<?php echo $key; ?>">Select</button>
              <input type="hidden" name="<?php echo $key; ?>" id="<?php echo $key; ?>" value="<?php echo $attach_id; ?>" />
              <div class="filename new"><span class="title"><strong><?php echo $filename; ?></strong></span></div>
            </div>
          </div>
        </div>

        <script>
          jQuery(function(){
            (function() {
                jQuery('.select-<?php echo $key; ?>').on('click', function(e){
                    window.self = this;
                    e.preventDefault();
               
                    // If the media frame already exists, reopen it.
                    if ( modal ) {
                      modal.open();
                      return;
                    }
                });

               var modal = wp.media({
                    title: 'Choose <?php echo $opts["label"]; ?>',
                    frame: 'select',
                    button: {
                              text: 'Select <?php echo $opts["label"]; ?>'
                          },
                          multiple: false,
                });

                modal.on( 'select', function() {
                  // We set multiple to false so only get one image from the uploader
                  attachment = modal.state().get('selection').first().toJSON();
                  
                  jQuery('#<?php echo $key; ?>').val(attachment.id);
                  jQuery('.row-<?php echo $key; ?> span.title strong').text(attachment.filename);
                 // jQuery('#media-item-<?php echo $key; ?> .pinkynail').attr('src', attachment.url)
                });

             })();
          });
        </script>

        <?php
      }



}

new SBM_Product_Metabox();