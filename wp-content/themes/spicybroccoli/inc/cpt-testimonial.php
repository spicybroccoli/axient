<?php
add_action( 'init', 'register_cpt_testimonial' );

function register_cpt_testimonial() {

    $labels = array( 
        'name' => _x( 'Testimonials', 'testimonial' ),
        'singular_name' => _x( 'Testimonial', 'testimonial' ),
        'add_new' => _x( 'Add New', 'testimonial' ),
        'add_new_item' => _x( 'Add New Testimonial', 'testimonial' ),
        'edit_item' => _x( 'Edit Testimonial', 'testimonial' ),
        'new_item' => _x( 'New Testimonial', 'testimonial' ),
        'view_item' => _x( 'View Testimonial', 'testimonial' ),
        'search_items' => _x( 'Search Testimonials', 'testimonial' ),
        'not_found' => _x( 'No testimonials found', 'testimonial' ),
        'not_found_in_trash' => _x( 'No testimonials found in Trash', 'testimonial' ),
        'parent_item_colon' => _x( 'Parent Testimonial:', 'testimonial' ),
        'menu_name' => _x( 'Testimonials', 'testimonial' ),
    );

    $args = array( 
        'labels' => $labels,
        'hierarchical' => false,
        
        'supports' => array( 'title', 'editor', 'thumbnail' ),
        
        'public' => true,
        'show_ui' => true,
        'show_in_menu' => true,
        
        'menu_icon'           => 'dashicons-megaphone',
        'show_in_nav_menus' => true,
        'publicly_queryable' => true,
        'exclude_from_search' => false,
        'has_archive' => true,
        'query_var' => true,
        'can_export' => true,
        'rewrite' => array('with_front' => false, 'slug' => 'testimonial'),
        'capability_type' => 'post'
    );

    register_post_type( 'testimonial', $args );
}

class SBM_Testimonial_Metabox {

    public function __construct() {
            add_action( 'add_meta_boxes', array( $this, 'add_meta_box' ) );
            add_action( 'save_post', array( $this, 'save' ) );
       
        }

        /**
         * Adds the meta box container.
         */
        public function add_meta_box( $post_type ) {
            add_meta_box( 'testimonialmetadiv', 'Testimonial Details', array( $this, 'render_meta_box_content' ), 'testimonial', 'advanced', 'high');
                
        }

        /**
         * Save the meta when the post is saved.
         *
         * @param int $post_id The ID of the post being saved.
         */
        public function save( $post_id ) {
        
            /*
             * We need to verify this came from the our screen and with proper authorization,
             * because save_post can be triggered at other times.
             */

            // Check if our nonce is set.
            if ( ! isset( $_POST['sbm_testimonial_nonce'] ) )
                return $post_id;

            $nonce = $_POST['sbm_testimonial_nonce'];

            // Verify that the nonce is valid.
            if ( ! wp_verify_nonce( $nonce, 'sbm_testimonial' ) )
                return $post_id;

            // If this is an autosave, our form has not been submitted,
                    //     so we don't want to do anything.
            if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) 
                return $post_id;

            // Check the user's permissions.
            if ( 'testimonial' == $_POST['post_type'] ) {

                if ( ! current_user_can( 'edit_page', $post_id ) )
                    return $post_id;
        
            } else {

                if ( ! current_user_can( 'edit_post', $post_id ) )
                    return $post_id;
            }

            /* OK, its safe for us to save the data now. */

            // Sanitize the user input.



            // Update the meta field.
            update_post_meta( $post_id, '_testimonial_author', sanitize_text_field( $_POST['testimonial_author']) );
            update_post_meta( $post_id, '_testimonial_author_job_title', sanitize_text_field( $_POST['testimonial_author_job_title']) );



      

        }


        /**
         * Render Meta Box content.
         *
         * @param WP_Post $post The post object.
         */
        public function render_meta_box_content( $post ) {

            // Enqueue media scripts
            wp_enqueue_scripts();
        
            // Add an nonce field so we can check for it later.
            wp_nonce_field( 'sbm_testimonial', 'sbm_testimonial_nonce' );

            // Display the form, using the current value.
            $this->input_field(array('key' => 'testimonial_author', 'heading' => 'Testimonial Author'));
            $this->input_field(array('key' => 'testimonial_author_job_title', 'heading' => 'Testimonial Author Job Title'));
        







            
       }

       private function features_field( $opts ) { ?>



          <?php  
          global $post;

          $key = 'features';
        $value = ( get_post_meta($post->ID, '_' . $key, true) ) ? get_post_meta($post->ID, '_' . $key, true) : '';

        
         ?>
            
           <div class="row row-features">
               

                    <?php if (!empty($value) && count($value) > 0) { 
                        for($i = 0; $i < count($value['text']); $i++) { ?>
                         <div class="single-feature">
                            <input type="text" value="<?php echo $value['image'][$i]; ?>" name="features[image][]" />
                            <textarea type="text" name="features[text][]"><?php echo $value['text'][$i]; ?></textarea>
                            <div class="remove-feature">&times;</div>
                            </div>
                        <?php }
                    } ?>            
           
           </div>
           <button class="add-features">Add</button>

           <script>
           jQuery(document).ready(function(){
                jQuery('.single-feature').on('click', '.remove-feature', function(e){
                    var parent = jQuery(this).parents('.single-feature').remove();
                })
                jQuery('.add-features').on('click', function(e){
                    console.log(e);
                    e.preventDefault(); 
                    var html = jQuery('<div class="single-feature"><input type="text" name="features[image][]" /><textarea type="text" name="features[text][]"></textarea><div class="remove-feature">&times;</div></div>');
                    jQuery('.row-features').append(html);
                })
           })
           </script>

      <?php }



      private function input_field( $opts ) { ?>
        <?php 

        global $post;
        $key = sanitize_title($opts['key']); 
        $value = ( get_post_meta($post->ID, '_' . $key, true) ) ? get_post_meta($post->ID, '_' . $key, true) : ''; 

        ?>

        <div class="row row-<?php echo $key; ?>">
          <label for="<?php echo $key; ?>">
            <?php echo $opts['heading']; ?>
          </label>
          <input type="text" id="<?php echo $key; ?>" name="<?php echo $key; ?>" value=" <?php echo esc_attr( trim($value) ); ?>" size="25" />
        </div>

      <?php
        // Reset $value
        $value = null;
      }

      private function textarea_field ( $opts ) { ?>
        <?php 

        global $post;
        $key = sanitize_title($opts['key']); 
        $value = ( get_post_meta($post->ID, '_' . $key, true) ) ? get_post_meta($post->ID, '_' . $key, true) : ''; 

        ?>

        <div class="row row-<?php echo $key; ?>">
          <label for="<?php echo $key; ?>">
            <?php echo $opts['heading']; ?>
          </label>
          <?php wp_editor($value, $key); ?>
        </div>

      <?php 
        // Reset $value
        $value = null;

      }

     



}

new SBM_Testimonial_Metabox();