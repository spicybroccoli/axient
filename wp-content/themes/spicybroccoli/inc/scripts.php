<?php
/**
 * Load Our Scripts
 *
 */

// Load Our Responsive Nav Scripts
add_action( 'wp_enqueue_scripts', function() {
	wp_enqueue_style('responsive-nav-css', get_stylesheet_directory_uri() .'/css/responsive-nav/responsive-nav.css');
	wp_enqueue_script('jquery');
	wp_enqueue_script('classie', get_stylesheet_directory_uri() . '/js/responsive-nav/classie.js');
	wp_enqueue_script('mlpushmenu', get_stylesheet_directory_uri() . '/js/responsive-nav/mlpushmenu.js');
	wp_enqueue_script('site', get_stylesheet_directory_uri() . '/js/site.js');

});
