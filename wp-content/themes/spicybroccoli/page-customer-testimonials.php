<?php
/**
 * Template Name: Testimonials
 *
 * @package WordPress
 * @subpackage Twenty_Ten
 * @since Twenty Ten 1.0
 */

get_header(); ?>

	<section class="panel panel-header">
		<img src="<?php echo get_stylesheet_directory_uri(); ?>/images/header_img.jpg">
	</section><!-- .panel -->

	<section class="panel panel-grey">
		<div class="wrapper">
			<?php get_sidebar('page'); ?>

			</pre>

			<div class="content">
				<div class="content-wrapper">
				<?php if(have_posts()) while(have_posts()): the_post(); ?>
					<h1><?php the_title(); ?></h1>
					<img src="<?php echo get_stylesheet_directory_uri(); ?>/images/testimonials.jpg">
					<div id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
					<?php the_content(); ?>
					</div>
				<?php endwhile; ?>

					<?php $testimonials = new WP_Query(array(
						'post_type' => 'testimonial',
						'posts_per_page' => -1,
						'orderby' => 'title',
						'order' => 'ASC'
						)); ?>
					<?php while ( $testimonials->have_posts() ) : $testimonials->the_post(); ?>
							<div id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
								
								<div class="entry-content">
									<?php 
										if ( has_post_thumbnail() ) { // check if the post has a Post Thumbnail assigned to it.
											the_post_thumbnail();
										} 
									?>
									<?php the_content(); ?>
									<span class="testimonial-company"><?php the_title(); ?></span>
									<span class="testimonial-author"><?php echo get_post_meta(get_the_ID(), '_testimonial_author', true); ?><?php echo (get_post_meta(get_the_ID(), '_testimonial_author_job_title', true)) ? ' | ' . get_post_meta(get_the_ID(), '_testimonial_author_job_title', true) : '' ; ?></span>
								</div><!-- .entry-content -->
								
							</div><!-- #post-## -->


					<?php endwhile; // End the loop. Whew. ?>
		
				</div>
			</div>
		</div>
	</section>

<?php get_footer(); ?>