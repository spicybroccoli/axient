<?php
/**
 * The main template file.
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Twenty_Ten
 * @since Twenty Ten 1.0
 */

get_header(); ?>

	<section class="panel panel-header">
		<img src="<?php echo get_stylesheet_directory_uri(); ?>/images/header_img.jpg">
	</section><!-- .panel -->

	<section class="panel panel-grey">
		<div class="wrapper">
			<?php get_sidebar('blog'); ?>

			<div class="content">
				<div class="content-wrapper">
				<?php while ( have_posts() ) : the_post(); ?>
					<h2><?php the_title(); ?></h2>
					<div class="entry-meta">
						<div class="entry-meta-author">
							<a href="<?php echo site_url('/blog/authors'); ?>"><?php echo get_the_author(); ?></a>
						</div> <span class="meta-sep">/</span> 
						<div class="entry-meta-date">
							<?php echo get_the_date(); ?>
						</div> <span class="meta-sep">/></span> 

						<?php
							$tags_list = get_the_tag_list( '', ', ' );
							if ( $tags_list ):
						?>
							<span class="tag-links">
								<?php printf( '<span class="%1$s">Tagged</span> %2$s', 'entry-utility-prep entry-utility-prep-tag-links', $tags_list ); ?>
							</span>
							
						<?php endif; ?>
					</div><!-- .entry-meta -->
					<?php 
						if ( has_post_thumbnail() ) { // check if the post has a Post Thumbnail assigned to it.
							the_post_thumbnail();
						} 
					?>

					<div class="entry-content">
						<?php the_content( 'Continue reading <span class="meta-nav">&rarr;</span>'); ?>
						<?php wp_link_pages( array( 'before' => '<div class="page-link">' .  'Pages:' , 'after' => '</div>' ) ); ?>
					</div><!-- .entry-content -->

					<!-- Go to www.addthis.com/dashboard to customize your tools -->
					<div class="addthis_sharing_toolbox"><span>Share it</span></div>

					<div class="entry-author">
						<?php global $post; ?>
						<?php $author = get_userdata($post->post_author); ?>
						<h3>About the Author</h3>
						<?php
							$local_avatars = get_user_meta( $post->post_author, 'simple_local_avatar', true );
							if ( !empty( $local_avatars['full'] ) ) {
								echo "<img src='" . esc_url( $local_avatars['96'] ) . "' class='avatar' />";
							}
						?>
						<!-- class="entry-author-img -->
						<h4><?php echo $author->display_name; ?></h4>
						<?php echo wpautop($author->description); ?>
						<a href="<?php echo get_user_meta($author->ID, 'linkedin', true); ?>" class="button">Connect via LinkedIn</a>
					</div><!-- .entry-author -->

				<?php endwhile; // End the loop. Whew. ?>
					
					
				</div><!-- .content-wrapper -->
			</div><!-- .content -->
		</div>
	</section><!-- .panel -->

<?php get_footer(); ?>