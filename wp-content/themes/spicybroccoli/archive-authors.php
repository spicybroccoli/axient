<?php
/**
 *
 * @package WordPress
 * @subpackage Twenty_Ten
 * @since Twenty Ten 1.0
 */

get_header(); ?>

	<section class="panel panel-header">
		<img src="<?php echo get_stylesheet_directory_uri(); ?>/images/header_img.jpg">
	</section><!-- .panel -->

	<section class="panel panel-grey">
		<div class="wrapper">
			<?php get_sidebar('blog'); ?>

			<?php $users = get_users(array('fields' => array('ID'), 'exclude' => (1))); ?>

			<div class="content">
				<div class="content-wrapper">
				<h1>About the Authors</h1>
				<?php foreach($users as $user): ?>
				<div class="entry-author authors-archive">
					<?php $author = get_userdata($user->ID); ?>
					<h3><?php echo $author->display_name; ?></h3>
					<?php
						$local_avatars = get_user_meta( $user->ID, 'simple_local_avatar', true );
						if ( !empty( $local_avatars['full'] ) ) {
							echo "<img src='" . esc_url( $local_avatars['96'] ) . "' class='avatar' />";
						}
					?>
					<!-- class="entry-author-img -->
					
					<?php echo wpautop($author->description); ?>
					<a href="<?php echo get_user_meta($author->ID, 'linkedin', true); ?>" class="button">Connect via LinkedIn</a>
				</div><!-- .entry-author -->
				<?php endforeach; ?>
				</div>
		
				</div>
			</div>
	</section>

<?php get_footer(); ?>